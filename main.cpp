#include <iostream>
#include "solve.hpp"

using namespace std;

int main() {
    int countOfRules;
    cin >> countOfRules;
    
    ErliAlgorithm CurrentGrammar;
    for (int i = 0; i < countOfRules; i++){
        // rules in format ...->....
        string Rule;
        cin >> Rule;
        CurrentGrammar.addRule(Rule);
    }
    
    string WordToCheck;
    cin >> WordToCheck;
    if (CurrentGrammar.CheckWord(WordToCheck))
        cout << "This word belong to grammar" << endl;
    else
        cout << "This word don't belong to grammar" << endl;
    
    
    return 0;
}
