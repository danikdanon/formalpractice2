#include "solve.hpp"
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

// строку преобразовываем в структуру правило
void ErliAlgorithm::addRule(const string &rule){
    Rule CurrentRule;
    CurrentRule.Left = rule[0];
    // строку без c-> в правую часть правила
    CurrentRule.Right = rule.substr(3,rule.size());
}

bool my_find(vector<Condition> &conditions, Condition &cond){
    for (int i = 0; i < conditions.size(); i++)
        if (conditions[i] == cond)
            return true;
    return false;
}

void AddCondition(vector<Condition> &conditions, Condition &cond){
    if (!my_find(conditions, cond)) // добавляем если нет
        conditions.push_back(cond);
}

// тупо все переписано с лекции
void ErliAlgorithm::Complete(int j, vector <vector<Condition>> &D){
    for(int i = 0; i < D[j].size(); i++){
        if (D[j][i].pointPos == D[j][i].Right.size()){
            for(int k = 0; k < D[D[j][i].NtCount].size(); k++){
                if(D[D[j][i].NtCount][k].Right[D[D[j][i].NtCount][k].pointPos] == D[j][i].Left){
                    Condition nCon = D[D[j][i].NtCount][k];
                    nCon.pointPos = D[D[j][i].NtCount][k].pointPos + 1;
                    AddCondition(D[j], nCon);
                }
            }
        }
    }
}

void ErliAlgorithm::Predict(int j, vector <vector<Condition>> &D, vector<Rule> &nRules){
    for(int i = 0; i < D[j].size(); i++){
        for(int g = 0; g < nRules.size(); g++){
            if (nRules[g].Left == D[j][i].Right[D[j][i].pointPos]){
                Condition nCon = Condition(nRules[g], 0, j);
                AddCondition(D[j], nCon);
            }
        }
    }
}

void ErliAlgorithm::Scan(int j, vector <vector<Condition>> &D, const string &word){
    for(int i = 0; i < D[j].size(); i++){
        if (D[j][i].Right[D[j][i].pointPos] == word[j]){
            Condition nCon = D[j][i];
            nCon.pointPos = D[j][i].pointPos + 1;
            AddCondition(D[j + 1], nCon);
        }
    }
}


bool ErliAlgorithm::CheckWord(const string &word){
    
    std::vector<Rule> CurrRules;
    CurrRules.push_back(Rule('$', rules[0].Left));
    
    for(int i = 0; i < rules.size(); i++){
        CurrRules.push_back(rules[i]);
    }
    
    std::vector <vector<Condition>> D(word.length() + 1);
    Condition InitCondition = Condition(Rule('$', rules[0].Left), 0, 0);
    AddCondition(D[0], InitCondition);
    // while D0 меняется
    int old_size = -1;
    while (old_size != D[0].size()) {
        old_size = D[0].size();
        Predict(0, D, CurrRules);
        Complete(0, D);
    };
    
    for(int j = 1; j <= word.length(); j++){
        Scan(j - 1, D, word);
        vector<Condition> Dj;
        
        // while Dj меняется
        int old_size = -1;
        while (old_size != D[j].size()) {
            old_size = D[j].size();
            Predict(0, D, CurrRules);
            Complete(0, D);
        };
    }
    
    bool ans = false;
    InitCondition.pointPos = 1;
    for(Condition curr : D.back())
        if (curr == InitCondition)
            ans = true;
    
    
    return ans;
}