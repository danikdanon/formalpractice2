#ifndef solve_hpp
#define solve_hpp

#include <stdio.h>
#include <string>
#include <vector>

struct Rule{
    Rule(const char L, const std::string R){
        Left = L;
        Right = R;
    }
    Rule(const char L, const char R){
        Left = L;
        Right = R;
    }
    Rule(){
        Left = '#';
        Right = "";
    }
    char Left;        // Left Side of the Rule
    std::string Right;    // Right side
};

struct Condition : Rule {
    int pointPos;     //сколько букв слева от точки
    int NtCount;     //терминалов перед нетерминалов
    
    Condition(const Rule &rule, const int &pointPosition, const int &NtCountArg){
        Left = rule.Left;
        Right = rule.Right;
        pointPos = pointPosition;
        NtCount = NtCountArg;
    }
    
    Condition(){
        pointPos = 0;
        NtCount = 0;
    }
    
    bool operator==(const Condition &Cond){
        return pointPos == Cond.pointPos && Left == Cond.Left && Right == Cond.Right && NtCount == Cond.NtCount;
    }
    
};

// не линкует падла, если так писать
/*bool operator==(Condition a, Condition b){
    return a.pointPos == b.pointPos && a.Left == b.Left && a.Right == b.Right && a.beforeIn == b.beforeIn;
}*/

struct ErliAlgorithm{
    void addRule(const std::string &rule);
    bool CheckWord(const std::string &word);       
    
    // D - обозначение с лекции D[j] - состояние, где прочитали j букаф
    void Scan(int j, std::vector <std::vector<Condition>> &D, const std::string &word);                       ///
    void Predict(int j, std::vector <std::vector<Condition>> &D, std::vector<Rule> &nRules);                    ///Для реализации алгоритма Эрли
    void Complete(int j, std::vector <std::vector<Condition>> &D);                   ///
    
    std::vector <Rule> rules;
};

#endif